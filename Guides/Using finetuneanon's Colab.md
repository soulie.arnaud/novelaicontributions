The following is a guide aimed at making it as easy and clear as possible to use [finetuneanon's GPT-NEO Dungeon](https://github.com/finetuneanon/gpt-neo_dungeon) Google Colab. If you don't know what this is or are somehow still having problems after following the guide's instructions, [visit the FAQ](#faq).

## Getting Started
There are a few steps you can take to better prepare ahead of time and save yourself some frustration later.
- A Google account is required to use the colab, but your personal information and data will not be made accessible to anyone just by running it. If you don't have a Google Drive account, sign up for one [here](https://www.google.com/drive/).
- If you intend on leaving the colab open for a while, it is recommended that you use a [tampermonkey](https://www.tampermonkey.net/) script like [this one](https://github.com/l-io-n/AI-Resources/raw/main/Novel%20AI/Tools/ColabAlive.user.js) to automatically reconnect whenever you are disconnected from the colab. Sometimes you will get a captcha you will need to fill out to stay connected and there is currently no way around that.
- If you don't use a tamper/greasemonkey script to auto reconnect or if doesn't work and you still get disconnected, just click `RECONNECT` and it should restore your session. Sometimes you may need to run the `Setup` cell again (or start over again from step 1) if for whatever reason disconnecting breaks the colab.


## Steps for Using the Colab
1) Visit the colab [here](https://colab.research.google.com/github/finetuneanon/gpt-neo_dungeon/blob/master/gpt-neo_dungeon.ipynb).
2) Read the instructions if you'd like.
3) Scroll down to the cell labeled `Setup` and click the play arrow.
<img src="https://user-images.githubusercontent.com/1778722/116846388-08c0cb80-ab9d-11eb-98d3-4213e391da34.png" width="500">

A warning may pop up, but just ignore it and click `RUN ANYWAY`.

<img src="https://user-images.githubusercontent.com/1778722/116846801-057a0f80-ab9e-11eb-8ec4-9e12671e6047.png" width="450">

A bunch of code will appear and some bars will start filling up. Just leave it be and wait until it is done.

<img src="https://user-images.githubusercontent.com/1778722/116847048-a072e980-ab9e-11eb-951f-f0293fd53d7c.png" width="700">

You can tell when the process has finished as the cell's play arrow will return back to how it was before you clicked it.

<img src="https://user-images.githubusercontent.com/1778722/116847207-edef5680-ab9e-11eb-9acb-82e200dc9ed6.png" width="300">

4) Scroll down to the next cell labeled `Model setup`. Here is where you choose which finetune model to use. The default is `horni` which is the model trained on literotica passages. If you want to instead use the `horni-ln` model (same as the `horni` model but with additional finetuning on light-novel passages) or the default GPT-Neo (with no finetuning), just click the drop down arrow over to the right and select the one you want.

<img src="https://user-images.githubusercontent.com/1778722/116847444-6229fa00-ab9f-11eb-9b7f-4e6265c982cb.png" width="950">

Once you know which model you want to use, click the play arrow. The model you selected will start to download to your instance of the colab. This download is **temporary** and will not be saved to your computer or Google Drive or take up any of your space. That said, if you want to download the model, see [Using a Model Hosted on Google Drive](#using-a-model-hosted-on-google-drive).

**The download will take a while (5-10 minutes on average)**. Even if it says it has 100% downloaded, it may not be completely finished. Once again, **wait for the cell's play arrow to turn back into an arrow**. If it is still a square, leave it alone.

> **OPTIONAL**<br />
> If you already have shortcuts to the models on your Google Drive or otherwise want to use another custom finetuned model, see [Using a Model Hosted on Google Drive](#using-a-model-hosted-on-google-drive).

Once the download is completed, it should look like this:

<img src="https://user-images.githubusercontent.com/1778722/116936412-a8716e80-ac1c-11eb-95ba-098d98524f4d.png" width="500">

5) Scroll *past* the optional next cell and down to the cell labeled `Sampling settings`. You can mess with these settings here if you want, but **it is not recommended unless you know what you are doing**. Click the play arrow and let the cell do its thing. It should only take a second.

6) The `Basic sampling` cell can let you test out any changes made to the previous `Sampling settings` cells, but is otherwise not necessary. You can ignore it if you wish and move on to the next step.

7) Scroll down a bit and read `Using the play function` to understand how to actually play with this model. I'm not going to go over these here as the information would be redundant.

When you're ready to play, scroll down to the cell labeled `Play` and click the play arrow. A small interface should appear in the cell:

<img src="https://user-images.githubusercontent.com/1778722/116937014-790f3180-ac1d-11eb-8b4b-aca82507c0b3.png" width="900">

This is where you can use the model as you would with something like AI Dungeon. If you want a "starting prompt", put some text into the `Input` field before hitting `[selected action]` which by default should already be set to `generate`. If you want an Author's Note for your story, click on the dropdown labeled `Action`, select `authorsnote`, place your Author's Note into the `Input` field and click `[selected action]`. After that, you can just continue to play with the AI as normal.

If you want the AI to keep a record of your story, run the cell labeled `History` by clicking the play arrow. It should continue to update automatically whenever you make more actions in your story.

That's it! Just make sure to save your story (easily done via the `History` cell) in a text file every once in a while and when you finish a play session as **this colab will not save your stories!**

You can leave the tab open, but chances are you will eventually get disconnected. If you *do* get disconnected, you can just click the `RECONNECT` button in the prompt that appears and continue as normal. **You do not need to refresh the page**. However, *colab can be very buggy* and you may need to run the `Setup` cell near the top of the page again to get things working again if they stop. If nothing works, you might just have to close out the tab and open it again via [finetuneanon's github](https://github.com/finetuneanon/gpt-neo_dungeon) and start all over again, so **make sure you backup your stories frequently**.


## Using a Model Hosted on Google Drive
This section describes how to keep a shortcut on your Google Drive to the horni/horni-ln models hosted on finetuneanon's drive. Shortcuts don't take up much space on your drive so this is a way to quickly load and use these models with the colab without having to download them each time or host them on your own drive.

If you want to use a custom finetune model that isn't the horni models or default gpt-neo, skip this section and continue [here](#loading-from-google-drive). Otherwise, navigate to the top of the colab where the instructions are written out. Look for the links to the `2.7B-horni` and `2.7B-horni-ln` models (the ones with `[Google Drive]` next to them).

<img src="https://user-images.githubusercontent.com/1778722/116850276-39a4fe80-aba5-11eb-9231-9d21fab69e9f.png" width="600">

Right click the `[Google Drive]` link next to the model you want and open it in a new tab. Make sure you are signed into your Google account.

In the new tab, look near the top right of the screen and click the icon labeled `Add shortcut to Drive`.

<img src="https://user-images.githubusercontent.com/1778722/116851123-b4bae480-aba6-11eb-91bd-832d148d798e.png" width="900">

A little box will pop up and show your Google Drive directory. Navigate to a place in your drive to add the shortcut if you want. Then click `Add Shortcut`.

<img src="https://user-images.githubusercontent.com/1778722/116851579-783bb880-aba7-11eb-8706-234ea9f054f3.png" width="250">

Now you should have a shortcut on your Google Drive to this finetune model. You can use this shortcut back in Step 4 to circumvent having to download it each time, though it still does take a few minutes to set up.

#### Loading from Google Drive
Follow the above [instructions](#steps-for-using-the-colab) up to Step 4. Once you reach the cell labeled `Model setup`, instead of downloading the models click the checkbox labeled `use_gdrive`. Make sure that the field labeled `model_gdrive` leads to where your hosted model (or shortcut to a hosted model) is. For instance, `/content/drive/MyDrive/` is the root folder of your Google Drive. If for instance you had a shortcut to the horni finetune in a folder called "AI Models" in your Google Drive, it would look like this: `/content/drive/MyDrive/AI Models/gpt-neo-2.7B-horni.tar`.

<img src="https://user-images.githubusercontent.com/1778722/116852253-d2894900-aba8-11eb-8742-99fa8c7b61d4.png" width="650">

Once the file path field is correct and the `use_gdrive` is checked, go ahead and click the play arrow for the `Model setup` cell. Text should appear below saying `Setting up model, this will take a few minutes` followed by a very long link and a empty field below labeled `Enter your authorization code:`. Click the link and it should open another tab asking you to sign into your Google account. Do so and you will get a request: `Google Drive for Desktop wants to access your Google Account`. Scroll down and click `Allow`. A long code should appear. Copy it to your clipboard and paste it back into that `Enter your authorization code` field back in the colab tab. Hit Enter.

------------

## FAQ
**Q)** What's this?

**A)** There's an open-source autoregressive language model called GPT-NEO which people are using for text adventures and literature co-authorship. To get more use out of GPT-NEO, users can finetune it with additional training data to create a model that produces more specifically curated output. Finetuneanon has created two such models dubbed "horni" (which is tuned with random passages from literotica) and "horni-ln" (tuned with some light novel content).
Normally models like these need to be hosted online somewhere (such as the case with AI Dungeon) or run locally on high-powered computers, but luckily Google provides its Colab service to freely host and run small applications like these up to a limited size. Thus, finetuneanon's Colab can function like a poor-anon's AI Dungeon or similar application, but with custom finetuned models.

**Q)** I got this error when trying to do Step 4 with my own finetune model / a shortcut to one of the `horni` models on my Google Drive: `Drive already mounted at /content/drive; to attempt to forcibly remount, call drive.mount("/content/drive", force_remount=True).`

**A)** You typed the directory filepath to the model / short incorrectly in the `model_gdrive` field. It won't let you change it without editing the code, but don't worry it's not that hard. At the very top right of that cell (the `Model setup` cell), look for a bunch of small icons that look like this:

<img src="https://user-images.githubusercontent.com/1778722/116853471-f8175200-abaa-11eb-88f6-ea2d99ac97f0.png" width="300">

Click the rightmost icon. A dropdown should appear. Hover over `Form` and click on `Show code`. You'll see the code for the cell. Find the section not far from the top that reads:

```
if use_gdrive:
  from google.colab import drive
  drive.mount('/content/drive', force_remount=True)
```

and change `drive.mount('/content/drive')` to `drive.mount('/content/drive', force_remount=True)` so it looks like this:

<img src="https://user-images.githubusercontent.com/1778722/116853742-66f4ab00-abab-11eb-952e-9f16196cd7c6.png" width="500">

Now try hitting the play arrow for the `Model setup` cell again.

