# NovelAI Community Repository
An early community contribution for the NovelAI project

## About this repository
It is an attempt to summarize and centralize community contributions for NovelAI  
It is maintained by Noli, Lion and Javaman, which are not official part of the NovelAI team  

## Who is this repo meant for
This repo is meant for anyone that wants to contribute to the community effort in the NovelAI project

## I want to contribute
Good! You can clone this repo and make a Merge Request (MR), We will be happy to get some activity here!

## But I don't know how to do it
You first have to install [GIT](https://git-scm.com/) and [configure it](https://git-scm.com/video/get-going)

- Using a CLI console, navigate to the folder where you want to download the repository
- Clone the repo: `git clone https://gitlab.com/nolialsea/novelaicontributions.git`
  - It will create the repository folder and download the default branch named `master`
  - If you feel adventurous, you can also clone with SSH: `git clone git@gitlab.com:nolialsea/novelaicontributions.git`
- Create a branch from master: `git checkout -b <name-of-your-branch>` (convention would be `username-followed-by-what-its-about` i.e. `noli-add-git-documentation`)
  - This branch is yours, you can commit and push as you will, it's not yet modifying the main branch (`master`)
- Make your modifications on the files using any editor
- Add your modification to the next commit: `git add .` to add every modified file, or `git add <file-name>` to only add one
- Commit your modifications on your branch: `git commit -m "your commit name explaining what it does or adds in a few words (~70char max)"`
- Push your modifications to your branch: `git push origin <name-of-your-branch>`
- From the website, go to the `Merge requests` section (on the left) and click "New merge request"
- Select your branch on the left, keep `master` branch on the right
- Write a title, by default it will be the commit name, if done correctly you don't have to change it
- Write a description if necessary
- (ideally) Put the `MR::Need review` label, if you omit it we will consider it as needing a review by default
- Click `Create merge request` button

If you use a git GUI, steps are basically the same

The `master` branch is protected and you can't touch it except by making a merge request

## Merge Request lifecycle
Once you send a Merge Request, it has to be approved by at least one maintainer before it can be merged  
You can send an MR even if the branch isn't finished yet, just prepend `Draft:` or `WIP:` to the MR name to block accidental merge until it's finished  
Sending your MR in `Draft` or `WIP` state is useful to get feedbacks earlier and avoid going too far into wrong paths

#### MR Labels
To help visualise MR states, labels are present to indicate some info:
- ~"MR::Need review" A review is needed, if no label is present, this is the default state
- ~"MR::Review OK" A review has been done with no problem found, requester can set the ~"MR::Ready to merge" label if they need it merged
- ~"MR::Fix needed" A review has been done, but they are problems to fix, requester should fix them then put the ~"MR::Need review" back
- ~"MR::Ready to merge" The branch is ready to be merged, but requires a review and approval in any case

#### MR Approval
Merge Requests need to be approved by at least one `approver` or `reviewer`, generally a repo maintainer  

#### MR Approval for maintainers
Maintainers cannot approve their own MR (Noli included), this is to enforce a validation by another maintainer  
They can however merge their own MR if it has been approved by another maintainer  
These mesures are necessary to keep everything as tidy and collaborative as possible  
Feel free to debate those decisions if you think we could do better otherwise

## On a side note
Note to official NovelAI team members that may read this, you are welcome on this repo. Collaboration and communication between the community collaborators and the official staff is important, the sooner the better
